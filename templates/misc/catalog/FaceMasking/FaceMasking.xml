<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\schema\pipeline.xsd"  xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
	<name>FaceMasking</name>
	<!--Should be  Name of the pipeline XML file -->
	<location>/data/intradb/pipeline/catalog/FaceMasking</location>
	<!-- Filesystem path to the pipeline XML -->
	<description>Pipeline creates face masked DICOM from high resolution MRI head DICOM scans</description>
	<documentation>
	   <authors>
	   	<author>
	   		<lastname>Milchenko</lastname>
			<firstname>Mikhail</firstname>
	   	</author>
	   </authors>
		<version>1</version>
		<input-parameters>
			<parameter>
				<name>scanids</name>
				<values><schemalink>xnat:mrSessionData/scans/scan/ID</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>xnat_id</name>
				<values><schemalink>xnat:mrSessionData/ID</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>sessionId</name>
				<values><schemalink>xnat:mrSessionData/label</schemalink></values>
				<description>The scan ids of all the scans of the session</description>
			</parameter>
			<parameter>
				<name>project</name>
				<values><schemalink>xnat:mrSessionData/project</schemalink></values>
				<description>Project</description>
			</parameter>
			<parameter>
				<name>subject</name>
				<values><schemalink>xnat:mrSessionData/subject_ID</schemalink></values>
				<description>Subject ID</description>
			</parameter>
			<parameter>
				<name>invasiveness</name>
				<values><csv>1.0</csv></values>
				<description>Invasiveness coefficient. Decrease/increase between 0.5 and 1.5 to control the degree of masking and depth of voxel alteration</description>
			</parameter>
		</input-parameters>
	</documentation>
	<xnatInfo appliesTo="xnat:mrSessionData"/>
	<outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/maskface')^</outputFileNamePrefix>
	<loop id="series" xpath="^/Pipeline/parameters/parameter[name='scanids']/values/list^"/>
	<!-- Description of the Pipeilne -->
	<parameters>
		<parameter>
			<name>mailhost</name>
			<values>
			  <unique>artsci.wustl.edu</unique>
			  </values>
		</parameter>
		<parameter>
			<name>workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text())^</unique>
			</values>
		</parameter>
	</parameters>
	<steps>
		<step id="0" description="Create a folder session folder" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools" >
				<!-- <argument id="p"/> -->
				<argument id="dirname">
					<value>MASKFACE</value>
				</argument>
			</resource>
		</step>
		<step id="1" description="Create folder for each series in MASKFACE subfolder" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE')^">
			<resource name="mkdir" location="commandlineTools" >
				<!-- <argument id="p"/> -->
				<argument id="dirname">
					<value>^PIPELINE_LOOPON(series)^</value>
				</argument>
			</resource>
		</step>
		<step id="0a" description="Create RAW folder" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools" >
				<!-- <argument id="p"/> -->
				<argument id="dirname">
					<value>RAW</value>
				</argument>
			</resource>
		</step>
		<step id="1a" description="Copy Scan DICOM DATA into RAW folder">
			<resource name="ArcGet" location="xnat_tools">
				<argument id="sessionId">
					<value>^/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text()^</value>
				</argument>
				<argument id="raw">
					<value>^PIPELINE_LOOPON(series)^</value>
				</argument>				
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="output">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW')^</value>
				</argument>
				<argument id="unzip">
					<value>true</value>
				</argument>
			</resource>
		</step>
		<step id="2" description="Run face masking on DICOM files" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^" >
			<resource name="mask_face" location="FaceMasking/resource">
                                <argument id="input">
                                	<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),'/SCANS/',PIPELINE_LOOPON(series),'/DICOM')^</value>
                                </argument>
				<argument id="output">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/DICOM')^</value>
				</argument>
				<argument id="zip" />
				<argument id="label">
					<value>^PIPELINE_LOOPON(series)^</value>
				</argument>
				<argument id="gc">
					<value>^/Pipeline/parameters/parameter[name='invasiveness']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		<step id="3" description="Upload defaced DICOM to XNAT" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/subjects/',/Pipeline/parameters/parameter[name='subject']/values/unique/text(),'/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(series),'/resources/DICOM_DEFACED/files/',PIPELINE_LOOPON(series),'.zip?format=DICOM&amp;content=DICOM_DEFACED&amp;extract=true"')^</value>
				</argument> 
				<argument id="local">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/',PIPELINE_LOOPON(series),'.zip')^</value>
				</argument>
			</resource>
		</step> 
		<step id="4" description="Upload defacing QC snapshots to XNAT" workdirectory="^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series))^">
			<resource name="XnatRestClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>^concat('"/data/archive/projects/',/Pipeline/parameters/parameter[name='project']/values/unique/text(),'/subjects/',/Pipeline/parameters/parameter[name='subject']/values/unique/text(),'/experiments/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(series),'/resources/DEFACE_QC/files/',PIPELINE_LOOPON(series),'.png?format=PNG&amp;content=MASKFACE_QC_PNG&amp;extract=true"')^</value>
				</argument> 
				<argument id="local">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/MASKFACE/',PIPELINE_LOOPON(series),'/maskface/',PIPELINE_LOOPON(series),'_qc.zip')^</value>
				</argument>				
			</resource>
		</step> 
		<step id="END-Notify" description="Notify">
			<resource name="Notifier" location="notifications">
                                <argument id="user">
                                        <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                                </argument>
                                <argument id="password">
                                        <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                                </argument>
				<argument id="to">
					<value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
				</argument>
				<argument id="cc">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="from">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^concat(/Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(), ' update: face masked DICOM files generated for ',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text() )^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
				</argument>
				<argument id="body">
					<value>^concat('Dear ',/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', ' Face masked DICOM files have been generated for  ', /Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),' . Details of the  session are available  &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'"&gt;', ' here. &lt;/a&gt; &lt;/p&gt;&lt;br&gt;', ' &lt;/p&gt;&lt;br&gt;', 'XNAT Team.')^
					</value>
				</argument>
			</resource>
		</step>
	</steps>
</Pipeline>
