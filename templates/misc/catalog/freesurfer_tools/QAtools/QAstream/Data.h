#ifndef DATA_H
#define DATA_H

#include <string>
#include <vector>
#include <gtk/gtk.h>

using std::string;
using std::vector;

//--- class SetData ---
//
//This class corresponds to an option in the second 
//window. It holds a pointer to a string in main
//named data, and a possible value for that data
//(it's corresponding options).  If this option is 
//selected, this class will set the data string to 
//the option's data.
//
class SetData {

 private:
    //the option's data
    string _setData;
    //the pointer to main's data string
    string *_data;
    //a pointer to the last set option
    static SetData *_lastSetOpt;

 public:
    SetData( string setData, string *data ): 
	_setData( setData )
	{ 
	    _data = data; 
	}
    SetData( char *setData, string *data ): 
	_setData( setData )
	{ 
	    _data = data;
	}
    //sets the data
    void set() { *_data = _setData; _lastSetOpt = this; }
    //returns true if this option was set last
    bool lastSet() { return ( _lastSetOpt != NULL && this == _lastSetOpt ); }
    void setNewData( string newdata ) { _setData = newdata; }
};


//--- class Data ---
//
//This class holds all of the data that is submitted 
//to the submit_data or submit_data_custom classes.
//
class Data {

 private: 
    GtkWidget *_window;
    GtkWidget *_subjectField;
    const int _choice;
    string &_data;
    string _filename;
    string _username;
    const vector<string*> *_customOpts;
    vector<int> _fieldWidths;

 public:
    Data( GtkWidget *window, GtkWidget *subjectField, int choice, string &data, string filename, string username ):
      _choice( choice ),
      _data( data ),
      _filename( filename ),
      _username( username )
    {
	_window = window;
	_subjectField = subjectField;
    }
    Data( GtkWidget *window, GtkWidget *subjectField, int choice, string &data, string filename, string username, const vector<string*> *customOpts, const vector<int> fieldWidths ):
      _choice( choice ),
      _data( data ),
      _filename( filename ),
      _username( username )
    {
	_window = window;
	_subjectField = subjectField;
	_customOpts = customOpts;
	vector<int>::const_iterator itr;
	for( itr = fieldWidths.begin(); itr != fieldWidths.end(); ++itr ) 
	    _fieldWidths.push_back( *itr );
    }
    GtkWidget * window() { return _window; }
    GtkWidget * subjectField() { return _subjectField; }
    int choice() { return _choice; }
    string data() { return _data; }
    string filename() { return _filename; }
    string username() { return _username; }
    const vector<string*> * customOpts() { return _customOpts; }
    int fieldwidth( int field ) { return _fieldWidths[field]; }
};

#endif
