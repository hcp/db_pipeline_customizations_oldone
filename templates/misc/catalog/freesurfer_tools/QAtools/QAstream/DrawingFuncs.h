#ifndef DRAWING_FUNCS_H
#define DRAWING_FUNCS_H

#include <string>
#include <vector>
#include <gtk/gtk.h>

using std::string;
using std::vector;

extern void pickOptions( GtkWidget *window, int &option );
extern GtkWidget * drawOptions( GtkWidget *window, int &option );
extern GtkWidget * drawQA( const int option, GtkWidget *window, string &choice_data, string filename, string user, const vector<string> * const subjectList );

#endif
