#ifndef CONFIG_H
#define CONFIG_H

//Various defined constants that affect all files
//are stored here

//Vertical spacing in GUI
#define VERT_SPACING 5

//Horizontal spacing in GUI
#define HORIZ_SPACING 20

//Default filename
#define FILENAME "QAnotes.qa"

//Default width of fields in text file
#define FIELDWIDTH 24

//Number of spaces between fields in text file
#define FIELD_PADDING 4

//Default number of fields in text file
// - don't change unless you change defaults
#define NUM_FIELDS 4

#endif
