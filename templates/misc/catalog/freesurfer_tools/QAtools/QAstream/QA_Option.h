#include <gtk/gtk.h>

//--- class QA_Option ---
//
// This class corresponds to a choice of category
// for the user on the inital button panel.  It
// also holds a pointer to the option variable in
// main(), that main will read.  When the function
// setTrue() is run, main()'s variable is set to
// the option number corresponding to the button
// which this object is associated with.
//
class QA_Option
{  
  private:
    GtkWidget *_window;
    //pointer to main()'s variable
    int *_option;
    //option number of associated button
    int _choice;

  public:
    QA_Option( GtkWidget *window ): _choice(0) { _option = 0; _window = window; }
    QA_Option( int choice, int* option, GtkWidget *window ): _choice( choice ) { _option = option; _window = window; }  
    GtkWidget *window() { return _window; }
    int option() { return *_option; }
    //sets main()'s variable
    void setTrue() { *_option = _choice; }
    
};

