#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <gtk/gtk.h>
#include <fstream>
#include <sstream>

#include "QA_Option.h"
#include "Data.h"
#include "CustomDrawingFuncs.h"
#include "GTKEventHandlers.h"
#include "Config.h"

using std::cout;
using std::cerr;
using std::endl;
using std::setw;
using std::left;
using std::string;
using std::ifstream;
using std::ofstream;
using std::istringstream;
using std::ostringstream;
using std::vector;
using std::iterator;

//------------------------
//--- Custom Functions ---
//------------------------

//Calls drawOptions, adds the options box into the window.
void pickOptions( GtkWidget *window, int &option, const vector<string*> customOpts )
{
    GtkWidget *options = drawOptions( window, option, customOpts );
    gtk_container_add( GTK_CONTAINER( window ), options );
}

//--- drawOptions ---
//
// Creates the initial category option panel based on the customOpts 
// vector.  Attaches the submit_option function to each category, 
// allowing the main function to read the choice of the user.
//
GtkWidget * drawOptions( GtkWidget* window, int &option, const vector<string*> customOpts )
{
    //creates new vertical box
    GtkWidget *vbox;
    vbox = gtk_vbox_new( TRUE, VERT_SPACING );
    
    //adds horizontal line separator to top of option panel
    GtkWidget *separator = gtk_hseparator_new();
    gtk_box_pack_start( GTK_BOX( vbox ), separator, TRUE, FALSE, 0 );
    gtk_widget_show( separator );

    GtkWidget *button;
    
    //defines an iterator to iterate through categories
    vector<string*>::const_iterator itr = customOpts.begin();
    //this is the option number assigned each category, beginning at 0.
    int optionNum = 0;
    while( itr != customOpts.end() ) {
	//create each button label from the first element in the array
	//stored at the element of customOpts pointed at by itr
        button = gtk_button_new_with_label( (*itr)[0].c_str() );
	//attach submit_option function to each button, store the optionNum of the category in the QA_Option object 
        g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( submit_option ), new QA_Option( optionNum, &option, window ));
        gtk_widget_show( button );

	//pack the button into the vbox
	gtk_box_pack_start( GTK_BOX( vbox ), button, TRUE, FALSE, 0 );
	++optionNum;
	++itr;
    }

    //ending line separator
    separator = gtk_hseparator_new();
    gtk_box_pack_start( GTK_BOX( vbox ), separator, TRUE, FALSE, 0 );
    gtk_widget_show( separator );
    gtk_widget_show( vbox );

    return vbox;
}

//--- drawQA ---
//
// Draws the custom options specified in customOpts for
// the category selected by the user from the initial
// panel of buttons.  Attach a SetData object to each radio
// button, along with the set_data function, which sets the
// data var in main() to the option chosen by the user. 
// Stores some important data in the Data object to be 
// passed to the submit_data_custom function, which is called 
// when the user hits the submit button.
//
// Note: customOpts is passed in as a pointer because
//       it must be stored in the Data object after the
//       function goes out of scope.
//
GtkWidget * drawQA( const int option, GtkWidget *window, string &choice_data, string filename, string user, const vector<string> * const subjectList, const vector<string*> * const customOptsPtr, const vector<string::size_type> customOptLen, const vector<int> fieldWidths )
{
    //initialize variables
    vector<string*> customOpts = *customOptsPtr;
    GtkWidget *mainVbox, *mainHbox, *pairVbox, *frame;
    //create the frame label from the category name
    string frameLabel = customOpts[option][0] + " Options";
    frame = gtk_frame_new( frameLabel.c_str() );
    //sets the first choice into choice_data as default
    choice_data = customOpts[option][1].c_str();

    mainVbox = gtk_vbox_new( TRUE, VERT_SPACING );
    mainHbox = gtk_hbox_new( TRUE, HORIZ_SPACING );

    GtkWidget *label, *rbutton, *freetext;
    GSList *rgroup = NULL;

    //Pack the labels and buttons in pairs, then into a horizontal box 
    for( int i = 1; i < static_cast<int>(customOptLen[option]); ++i ) {
	//create new pair
	pairVbox = gtk_vbox_new( TRUE, VERT_SPACING );
	//check if the option is a freetext option
	if( customOpts[option][i] == "$freetext" ) {	      
	    //create a new radio button from previous button's group
	    rbutton = gtk_radio_button_new( rgroup );
	    //set rgroup to this button's group, for next button
	    rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	    //set the default text to the null value
	    SetData *freetextData = new SetData( "-", &choice_data );
	    //attach set_data to rbutton, to set main's data var to the data in the freetext box
	    g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), freetextData );
	    //create a new text entry
	    freetext = gtk_entry_new_with_max_length( FIELDWIDTH - FIELD_PADDING );
	    //update the SetData data every time the text in the box is changed
	    g_signal_connect( G_OBJECT( freetext ), "changed", G_CALLBACK( set_freetext_data ), freetextData );
	    //pack the entry field and the button into a pair
	    gtk_box_pack_start( GTK_BOX( pairVbox ), freetext, TRUE, FALSE, 0 );
	    gtk_widget_show( freetext );	
	    gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, TRUE, 0 );
	    gtk_widget_show( rbutton );
	    //pack the pair into the main horizontal box
	    gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	    gtk_widget_show( pairVbox );
	}
	else {
	    //create a label and pack it into the pair
	    label = gtk_label_new( customOpts[option][i].c_str() );	
	    gtk_box_pack_start( GTK_BOX( pairVbox ), label, TRUE, FALSE, 0 );
	    gtk_widget_show( label );
	    //create a new radio button from previous button's group.
	    rbutton = gtk_radio_button_new( rgroup );
	    //set rgroup to this button's group, for the next button
	    rgroup = gtk_radio_button_get_group( GTK_RADIO_BUTTON( rbutton ) );
	    //attach set_data to rbutton, to set main's data var to the specified option from customOpts
	    g_signal_connect( G_OBJECT( rbutton ), "clicked", G_CALLBACK( set_data ), new SetData( customOpts[option][i], &choice_data ));
	    //pack the button into the pair
	    gtk_box_pack_start( GTK_BOX( pairVbox ), rbutton, TRUE, FALSE, 0 );
	    gtk_widget_show( rbutton );	
	    //pakc the pair into the main horizontal box
	    gtk_box_pack_start( GTK_BOX( mainHbox ), pairVbox, TRUE, FALSE, 0 );
	    gtk_widget_show( pairVbox );
	}
    }
    //pakc the main horizontal box into the main vertical box
    gtk_box_pack_start( GTK_BOX( mainVbox ), mainHbox, TRUE, FALSE, 0 );
    gtk_widget_show( mainHbox );

    GtkWidget* subjectField;

    //if there is no list of subjects, create a text entry
    if( subjectList->empty() ) {
	subjectField = gtk_entry_new();
	//set the default text to Enter_Subject (not just Subject b/c the column header is Subject)
	gtk_entry_set_text( GTK_ENTRY( subjectField ), "Enter_Subject" );
	//select the default text, so the user can replace it quickly
	gtk_editable_select_region ( GTK_EDITABLE( subjectField ), 0, GTK_ENTRY( subjectField )->text_length );
	//pack this into the main vbox, below the options
	gtk_box_pack_start( GTK_BOX( mainVbox ), subjectField, TRUE, FALSE, 0 );
	gtk_widget_show( subjectField );
    }
    else {
	//if there is a list of subjects, create a combo box (drop down menu)
	GtkWidget* combo = gtk_combo_new();    
	GList *comboList = NULL;
	
	//fill the drop down list with the subjects
	vector<string>::const_iterator itr;
	for( itr = subjectList->begin(); itr != subjectList->end(); ++itr )
	    comboList = g_list_append( comboList, const_cast<char*>( itr->c_str() ) );
	//set the combo box to use the list of subjects
	gtk_combo_set_popdown_strings (GTK_COMBO (combo), comboList);
	
	//pack this into the main vbox, below the options
	gtk_box_pack_start( GTK_BOX( mainVbox ), combo, TRUE, FALSE, 0 );
	gtk_widget_show( combo );
	//set the subject field to the text entered in the box
	subjectField = GTK_COMBO( combo )->entry;
    }

    //create the submit button
    GtkWidget* submit = gtk_button_new_with_label( "Submit" );
    //connect the submit_data_custom function to the button, along with a Data object with all the data submit_data_custom needs
    g_signal_connect( G_OBJECT( submit ), "clicked", G_CALLBACK( submit_data_custom ), new Data( window, subjectField, option, choice_data, filename, user, customOptsPtr, fieldWidths ) );
    //pack the submit button in the vbox below the options and the subjects
    gtk_box_pack_start( GTK_BOX( mainVbox ), submit, TRUE, FALSE, 0 );
    gtk_widget_show( submit );

    //put the main vbox into the frame, and return the frame.
    gtk_container_add( GTK_CONTAINER( frame ), mainVbox );
    gtk_widget_show( mainVbox );
    gtk_widget_show( frame );
    return frame;
}

//--- submit_data_custom ---
//
// This function takes the custom data from drawQA and 
// reads it into a text file.  It will update the text
// file if it finds that the subject it is updating is
// there.  It will append a new subject onto the file
// if it not there.
//
void submit_data_custom( GtkWidget *button, gpointer data )
{
    //read the Data object into choice_data
    Data *choice_data = static_cast<Data*>(data);
    //get the filename to read to, and the user
    string filename = choice_data->filename();
    string user = choice_data->username();
    //the data from the file will be read into raw_data
    vector<string> raw_data;
    //the filehandle, aka file stream, for reading a file
    ifstream infile;
   
    //attempt to open the file for reading
    infile.open( filename.c_str() );
    if( !infile ) {
	//if it doesn't open, most likely it doesn't exist, so create it
        cout << "Unable to open input file: " << filename << endl;
        cout << "Creating " << filename << "..." << endl;
	ofstream newfile( filename.c_str() );
	//if we can't create it, then die
	if( !newfile )
	    cerr << "Error: Unable to create file: " << filename << endl;
	else {
	    //Draw the headers in, from the first element of each array in customOpts
	    //setw sets the number of spaces for each field.  FIELDWIDTH is used for the
	    //Subject and User fields, as well as the default fields if no custom opts are
	    //used (in DrawingFuncs.cpp).  FIELDWIDTH is defined in Custom.h
	    newfile << left << setw(FIELDWIDTH) << "Subject"
		    << setw(FIELDWIDTH) << "User";
	    for( int i = 0; i < static_cast<int>( choice_data->customOpts()->size() ); ++i ) {
	        newfile << left << setw(choice_data->fieldwidth(i)) << (*(choice_data->customOpts()))[i][0];
	    }
	    newfile << "\n";
	    //get subject from Data object (from subject field)
	    string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
	    //big comments, so I can find where the data is written
	    //##############################
	    //### WRITE DATA TO NEW FILE ###
	    //##############################
	    //write in subject and user data
	    newfile << left 
		    << setw(FIELDWIDTH) << subject
		    << setw(FIELDWIDTH) << user;
	    for( int pos = 0; pos < static_cast<int>( choice_data->customOpts()->size() ); ++pos ) {
		//if this position is our new data
	        if( pos == choice_data->choice() )
		    //write in our new data with custom fieldwidth, defined in main()
		    newfile << left << setw(choice_data->fieldwidth(pos)) << choice_data->data();
		else
		    //otherwise, write in null with custom fieldwidth.
		    //"-" character used for null so submit_data_custom
		    //recognizes that there is a field there.
		    newfile << left << setw(choice_data->fieldwidth(pos)) << "-";
	    }
	    newfile << endl;
	    newfile.close();
	}
    }
    //if we can open the file
    else {
        int nextIndex = 0;
	string line;
	//read in all the data from the file, one line at a time
	while( !infile.eof() ) {
	    //getline reads everything up to the newline
	    getline( infile, line );
	    //if the line is blank, don't read it in.
	    //Blank lines screw up the updating mechanism
	    if( line == "" )
	        continue;
	    //otherwise, put the newline back on and 
	    //store the line in raw_data
	    line += "\n";
	    raw_data.push_back( line );
	    ++nextIndex;
	}
	//close our input file
	infile.close();

	//debug option - print out the raw data
	//vector<string>::iterator dItr;
	//for( dItr = raw_data.begin(); dItr != raw_data.end(); ++dItr )
	//    cout << *dItr;
	//cout << endl;

	//get subject from Data object (from subject field)
	string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
	vector<string>::iterator itr;
	//the input string stream is for reading lines out one word at a time
	istringstream strm;
	//the output string stream is to hold our updated line
	ostringstream newline;
	string word;
	bool wordFound = false;
	//using our raw_data vector, update the data within the system first
        for( itr = raw_data.begin(); itr != raw_data.end(); ++itr ) {
	    //set the string stream
	    strm.str( itr->c_str() );
	    //read out the subject
	    strm >> word;
	    //if the subject matches the one we're looking for, update the line
	    if( word == subject ) {
		//####################################
		//### UPDATE DATA IN EXISTING FILE ###
		//####################################
		//write subject to the new line
	        newline << left << setw(FIELDWIDTH) << word;
		//read the old user, write the current user
		strm >> word;
		newline << left << setw(FIELDWIDTH) << user;
		//while we haven't gotten to our new data yet
		for( int i = 0; i < choice_data->choice(); ++i ) {
		    //read the data out...
		    strm >> word;
		    //..and then write it back in to our new line
		    newline << left << setw(choice_data->fieldwidth(i)) << word;
		}
		//now at our new data, read out the old data...
		strm >> word;
		//...and write in the new
		newline << left << setw(choice_data->fieldwidth(choice_data->choice())) << choice_data->data();
		for( int i = choice_data->choice() + 1; i < static_cast<int>( choice_data->customOpts()->size() ); ++i ) {
		    //copy the rest of the data back into the new line
		    strm >> word;
		    newline << left << setw(choice_data->fieldwidth(i)) << word;
		}
		//append a newline to the end of our new line (no pun intended)
		newline << endl;
		//convert our stringstream back to a string, and store it
		//in our raw_data vector
		*itr = newline.str();
		//note that we found the word
		wordFound = true;
	        break;
	    }
	}
	//file stream for output
	ofstream outfile;
	//if we found the word
	if( wordFound ) {
	    //attempt to open file (as a blank file, 
	    //clobbering everything already there)
	    outfile.open( filename.c_str() );
	    //if it doesn't open, die
	    if( !outfile )
	        cerr << "Error: Unable to write to file: " << filename << endl;
	    //else, write raw_data back into the file
	    else {
	        for( itr = raw_data.begin(); itr != raw_data.end(); ++itr )
		    outfile << *itr;
		outfile.close();
	    }
	}
	else {
	    //open for append, saving file contents
	    outfile.open( filename.c_str(), ofstream::app );
	    //if it doesn't open, die
	    if( !outfile )
	        cerr << "Error: Unable to write to file: " << filename << endl;
	    //else, append new data on
	    else {
		//get subject from Data object (from the subject field)
	        string subject( static_cast<const char*>( gtk_entry_get_text( GTK_ENTRY( choice_data->subjectField() ) ) ) );
		//#####################################
		//### ADD NEW DATA TO EXISTING FILE ###
		//#####################################
		//write subject and user
		outfile << left 
			<< setw(FIELDWIDTH) << subject
			<< setw(FIELDWIDTH) << user;
		for( int pos = 0; pos < static_cast<int>( choice_data->customOpts()->size() ); ++pos ) {
		    //if its our new data, write the data, otherwise write null
		    if( pos == choice_data->choice() )
			outfile << left << setw(choice_data->fieldwidth(pos)) << choice_data->data();
		    else
			outfile << left << setw(choice_data->fieldwidth(pos)) << "-";
		}
		//append newline and close
		outfile << endl;
		outfile.close();
	    }
	}
    }

    //delete (hide) the option window (also quits main loop)
    delete_window( GTK_WIDGET( choice_data->window() ), NULL );
}
