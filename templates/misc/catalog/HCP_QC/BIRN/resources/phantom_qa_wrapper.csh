#!/bin/csh

#This is a wrapper script to generate the QCAssessment element for the Phantom QA
#using the BIRN Phantom QA techinique
#Refer: https://xwiki.nbirn.net/xwiki/bin/view/Function-BIRN/AutomatedQA

if ( ${#argv} < 3 ) then
  echo "Usage: phantom_qa_wrapper <XNAT ID>  <Project Id> <Path to the QCAssessment File>"
  exit -1;
endif

set xnatId=$1
set project=$2
set qcAssessmentFile=$3
set qc_id=$4
set roisize=$5
set host=$6

set siteId = "1"
set siteName = "HCPIntraDB"



set today=`date +"%Y-%m-%d"`


echo $today

echo "<xnat:QCAssessment ID="\"$qc_id\"" type="\""BIRN_AGAR_QC"\"" project="\""$project"\"" label="\"$qc_id\"" xmlns:xnat="\""http://nrg.wustl.edu/xnat"\""  xmlns:xsi="\""http://www.w3.org/2001/XMLSchema-instance"\""  xsi:schemaLocation="\""http://nrg.wustl.edu/xnat https://cndabeta.wustl.edu/schemas/xnat/xnat.xsd"\"">" > $qcAssessmentFile

echo "<xnat:date>$today</xnat:date>" >> $qcAssessmentFile
echo "<xnat:imageSession_ID>$xnatId</xnat:imageSession_ID>"  >> $qcAssessmentFile

echo "<xnat:scans>" >> $qcAssessmentFile

foreach s (`ls -d */ | xargs -l basename`)
	set imagefile = $cwd/$s"_image.xml"
	set scanId = `echo $s | sed "s/scan//"`
	@PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/phantom_qa.csh $siteId $siteName $imagefile $qcAssessmentFile $scanId $roisize $xnatId $qc_id $host
	if ($status) exit 1
end


echo "</xnat:scans>" >> $qcAssessmentFile
echo "</xnat:QCAssessment>" >> $qcAssessmentFile

exit 0
