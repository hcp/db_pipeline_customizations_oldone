#!/bin/csh 
set echo 
set session = $argv[1]
set wrkdir = $argv[2]
set outdir = $argv[3]
set scanid = $argv[4]



		foreach fname ( $wrkdir/*_b0Statistics.txt )
			set filename = `basename $fname .txt`
			tail -n +2 $fname |  cut -d" " -f2 > $outdir/${filename}_stripped.dat
			gnuplot -persist <<PLOT
			set terminal gif small size 640,480 \\
					       xffffff x000000 x404040 \\
					       xff0000 xffa500 x66cdaa xcdb5cd \\
					       xadd8e6 x0000ff xdda0dd x9500d3    # defaults
			set key inside left top vertical Right noreverse enhanced box linetype -1 
			set xlabel "Slice"
			set ylabel "B0Mean"
			set output '$outdir/${filename}_b0Mean.gif'
			plot '$wrkdir/${filename}_stripped.dat' using 1:2 title "Slice vs B0Mean"  with lines

			set ylabel "B0Std"
			set output '$outdir/${filename}_b0Std.gif'
			plot '$wrkdir/${filename}_stripped.dat' using 1:3 title "Slice vs B0Std"  with lines


			set terminal gif small size 300,300 \\
					       xffffff x000000 x404040 \\
					       xff0000 xffa500 x66cdaa xcdb5cd \\
					       xadd8e6 x0000ff xdda0dd x9500d3    # defaults
			set ylabel "B0Mean"
			set output '$outdir/${filename}_b0Mean_thumb.gif'
			plot '$wrkdir/${filename}_stripped.dat' using 1:2 title "Slice vs B0Mean"  with lines

			set ylabel "B0Std"
			set output '$outdir/${filename}_b0Std_thumb.gif'
			plot '$wrkdir/${filename}_stripped.dat' using 1:3 title "Slice vs B0Std"  with lines

			quit
			PLOT
			

		endif

exit 0